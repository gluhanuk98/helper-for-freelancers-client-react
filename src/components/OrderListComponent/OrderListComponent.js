import React, {useEffect, useState} from 'react';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Box from "@mui/material/Box";
import axios from 'axios';
import {CircularProgress} from "@mui/material";
import Pagination from '@mui/material/Pagination';
import usePagination from "../../context/Pagination";

const OrderListComponent = () => {
    const [order, setOrder] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);
    const pageSize = 5;

    const count = Math.ceil(order.length / pageSize);
    const data = usePagination(order, pageSize);

    const handleChange = (event, value) => {
        console.log(value)
        setPageNumber(value);
        data.jump(value);
    }

    const getOrderList = async () => {
        setLoading(true)
        try {
            const order = await axios.get(`http://localhost:8081/api/orders/all`)
            setOrder(order.data)
        } catch (e) {
            setError(e.message)
        } finally {
            setLoading(false)
        }
    }
    useEffect(() => {
        getOrderList()
    }, []);

    if (loading) {
        return (<div
                style={{
                    display: "flex", justifyContent: "center", alignItems: "center", padding: 30,
                }}
            >
                <CircularProgress/>
            </div>);
    }
    if (error) {
        return (<>
                <h5>{error}</h5>
            </>);
    }

    const OrderViewList = () => {
        if (data) {
            return (data.currentData().map(el => (<>
                        <CardContent key={el.id}>
                            <Typography sx={{fontSize: '16px'}}>
                                {el.user.firstName} {el.user.lastName}
                            </Typography>
                            <Box sx={{justifyContent: 'space-between', flexDirection: 'row', display: 'flex'}}>
                                <Typography variant="h5" component="div">
                                    {el.title}
                                </Typography>
                                <Typography variant="h5" component="div">
                                    {el.price} UAH
                                </Typography>
                            </Box>
                            <Typography sx={{mb: 1.5}} color="text.secondary">
                                {el.categories}
                            </Typography>
                            <Typography variant="body2">
                                {el.description}
                            </Typography>
                            <Typography sx={{fontSize: 14}} color="text.secondary" gutterBottom>
                                {el.startDate} - {el.endDate}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button variant="container"
                                    sx={{fontSize: '10px', backgroundColor: "green", color: "white"}}
                                    href={`/orders/${el.id}`}>Read
                                more...</Button>
                        </CardActions>
                        <hr/>
                    </>)))
        } else {
            return <Typography>Сreate first order</Typography>
        }
    }

    return (<Box sx={{maxWidth: 825, margin: '0 auto', mt: 5}}>
            <OrderViewList/>
            <Pagination count={count} page={pageNumber} onChange={handleChange}

            />
        </Box>);
}

export default OrderListComponent;